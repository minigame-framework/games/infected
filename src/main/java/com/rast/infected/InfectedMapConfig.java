package com.rast.infected;

import com.rast.gamecore.MapConfig;
import com.rast.gamecore.util.Region;
import org.bukkit.Location;

import java.util.List;

public class InfectedMapConfig extends MapConfig {
    private final List<Region> spawnRegions;
    private final int minPlayers;

    public InfectedMapConfig(String name, Location mainSpawn, int minPlayers, int maxPlayers, List<Region> spawnRegions) {
        super(name, maxPlayers, mainSpawn);
        this.minPlayers = minPlayers;
        this.spawnRegions = spawnRegions;
    }

    public List<Region> getSpawnRegions() {
        return spawnRegions;
    }

    public int getMinPlayers() {
        return minPlayers;
    }
}

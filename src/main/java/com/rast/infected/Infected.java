package com.rast.infected;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameInstanceManager;
import com.rast.gamecore.util.Kits;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class Infected extends JavaPlugin {

    private static Infected plugin; // this plugin
    private static Settings settings; // the settings for this game
    private static InfectedGame infectedGame; // the game object that gets registered wth GameCore
    private static GameInstanceManager instanceManager;
    private static File templateFolder; // the folder for the maps
    private static PlayerTags playerTags; // the player tags
    private static Kits kits;

    // the plugin getter
    public static Infected getPlugin() {
        return plugin;
    }

    // the settings getter
    public static Settings getSettings() {
        return settings;
    }

    // the getter for the plugin's game. the name of this should be changed to (Plugin Name + "Game")
    public static InfectedGame getInfectedGame() {
        return infectedGame;
    }

    // the template folder getter
    public static File getTemplateFolder() {
        return templateFolder;
    }

    // the player tags getter
    public static PlayerTags getPlayerTags() {
        return playerTags;
    }

    public static Kits getKits() {
        return kits;
    }

    public static GameInstanceManager getInstanceManager() {
        return instanceManager;
    }

    @Override
    public void onEnable() {
        plugin = this; // get this plugin instance
        settings = new Settings(); // load the settings
        playerTags = new PlayerTags(); // create a player tags instance

        // setup the map template folder
        templateFolder = new File(plugin.getDataFolder().getAbsoluteFile() + "/maps/");
        if (!templateFolder.exists()) {
            if (!templateFolder.mkdirs()) {
                getLogger().warning("was unable to create the path (" + templateFolder.getAbsolutePath() + ")");
            }
        }

        infectedGame = new InfectedGame("Infected", Arrays.asList(Objects.requireNonNull(templateFolder.list())), false, this); // create a new game instance which should only be made once
        infectedGame.addMapConfigs(settings.getMapConfigs());
        instanceManager = new GameInstanceManager(infectedGame);
        kits = new Kits(this, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Kit Menu", Material.CHEST, false,
                ChatColor.YELLOW + "" + ChatColor.BOLD + "Kit Menu");

        GameCore.getGameMaster().registerGame(infectedGame); // register the game with GameCore
        GameCore.getKitsManager().registerKits(infectedGame, kits);

        // create a player group for the game
        // player groups are used by GameCore to determine who can see what chat
        GameCore.getGameMaster().createPlayerGroup(infectedGame.getName());

        // register events
        getServer().getPluginManager().registerEvents(new Events(), this);
        Objects.requireNonNull(plugin.getCommand("infected")).setExecutor(new Commands());
    }

    @Override
    public void onDisable() {
        instanceManager.purgeInstances();
    }
}

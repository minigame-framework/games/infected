package com.rast.infected;

import com.rast.gamecore.GameInstance;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameStatus;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.scores.ScoreFunction;
import com.rast.gamecore.scores.ScoreManager;
import com.rast.gamecore.util.BroadcastWorld;
import com.rast.gamecore.util.CleanPlayer;
import com.rast.gamecore.util.Region;
import org.bukkit.*;
import org.bukkit.SoundCategory;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public class InfectedInstance extends GameInstance {

    // variables
    private final int maxPlayers;
    private final int minPlayers;

    // bossbars
    private final BossBar startCountdownBar;
    private final BossBar survivorWinCountdownBar;

    // Player groups
    private final Set<Player> waitingPlayers = new HashSet<>();
    private final Set<Player> alivePlayers = new HashSet<>();
    private final Set<Player> infectedPlayers = new HashSet<>();
    private Player hiddenInfected;
    private final Set<Player> respawnPlayers = new HashSet<>();
    private final Set<Player> deadPlayers = new HashSet<>();

    // game flags
    private boolean gracePeriod = false;
    private boolean gameStarted = false;
    private boolean gameEnded = false;
    private boolean pvpEnabled = false;
    private boolean infectedFound = false;

    // start timer and grace timer settings
    private long startTimerCount = 0;
    private long gracePeriodTime = 0;
    private BukkitTask startCounter;
    private BukkitTask gracePeriodCounter;

    // respawn timers
    private final HashMap<Player, RespawnTimer> respawnTimers = new HashMap<>();

    // survivor time counter
    private final long initialSurvivorWinTime = Infected.getSettings().getGameLength();
    private long survivorWinTime = 0;
    private BukkitTask winCounterClock;

    // score tallies
    private final HashMap<Player, Integer> survivorKills = new HashMap<>(); // amount of survivors killed
    private final HashMap<Player, Integer> infectedKills = new HashMap<>(); // amount of infected killed

    // despawn timer object
    private BukkitTask worldDespawnTimer;

    // get the variables for the game
    public InfectedInstance(GameWorld gameWorld) {
        super(gameWorld);
        maxPlayers = Infected.getSettings().getMapConfig(getGameWorld().getMap()).getMaxPlayers();
        minPlayers = Infected.getSettings().getMapConfig(getGameWorld().getMap()).getMinPlayers();
        startCountdownBar = Bukkit.createBossBar(ChatColor.YELLOW + "Waiting for players...", BarColor.YELLOW, BarStyle.SOLID);
        startCountdownBar.setProgress(1.0);
        survivorWinCountdownBar  = Bukkit.createBossBar(ChatColor.RED + "Survivors win in " + initialSurvivorWinTime + "s", BarColor.RED, BarStyle.SOLID);
        startCountdownBar.setProgress(1.0);
    }

    // add player
    public void addPlayer(Player player) {
        // first ensure that the game is waiting open and the match does not have the player
        if (getGameWorld().getStatus() == GameStatus.WAITING_OPEN && !hasPlayer(player)) {
            // send the player to the game
            waitPlayerPrep(player);
            startCountdownBar.addPlayer(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName()
                    + " has joined the match. " + ChatColor.DARK_GRAY + '(' + getPlayerCount() + '/' + maxPlayers + ')');
            startCountdown();
        }
        worldStatusRefresh();
    }

    public void removePlayer(Player player) {
        if (hasPlayer(player)) {
            // remove the tags that may have been assigned to the player
            waitingPlayers.remove(player);
            alivePlayers.remove(player);
            infectedPlayers.remove(player);
            deadPlayers.remove(player);
            respawnPlayers.remove(player);
            startCountdownBar.removePlayer(player);
            survivorWinCountdownBar.removePlayer(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName() + " has left the match.");
            if (!isGameReady()) {
                stopStartCountdown();
            }
            worldStatusRefresh();
            saveScoreTallies(player);
            if (respawnTimers.containsKey(player)) {
                respawnTimers.remove(player).stopTask();
            }
            gameWinCheck();
        }
    }

    // prep the player for waiting
    private void waitPlayerPrep(Player player) {
        waitingPlayers.add(player);
        alivePlayers.remove(player);
        infectedPlayers.remove(player);
        deadPlayers.remove(player);
        respawnPlayers.remove(player);
        startCountdownBar.removePlayer(player);
        player.setGameMode(GameMode.ADVENTURE);
        Location loc = Infected.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        player.getInventory().clear();
    }

    // prep the player as a survivor
    private void survivorPlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        alivePlayers.add(player);
        infectedPlayers.remove(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        player.teleport(getRandomSpawn());
        CleanPlayer.cleanAll(player);
        player.getInventory().setContents(Infected.getKits().loadKit("Survivor").getContents());
        gameWinCheck();
    }

    // prep the player as a hidden infected
    private void infectedPlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        alivePlayers.remove(player);
        infectedPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        player.teleport(getRandomSpawn());
        CleanPlayer.cleanAll(player);
        player.getInventory().setContents(Infected.getKits().loadKit("Infected").getContents());
        gameWinCheck();
    }

    // prep the player as an infected
    private void hiddenInfectedPlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        alivePlayers.remove(player);
        infectedPlayers.remove(player);
        hiddenInfected = player;
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        player.teleport(getRandomSpawn());
        CleanPlayer.cleanAll(player);
        player.getInventory().setContents(Infected.getKits().loadKit("Hidden-Infected").getContents());
        gameWinCheck();
    }

    // prep the player for the game
    private void respawnPlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        alivePlayers.remove(player);
        infectedPlayers.remove(player);
        respawnPlayers.add(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = Infected.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        respawnTimers.put(player, new RespawnTimer(player, 5, () -> {
            if (!gameEnded) {
                infectedPlayerPrep(player);
            }
        }));
    }

    // prep the player for spectate
    private void spectatePlayerPrep(Player player) {
        alivePlayers.remove(player);
        waitingPlayers.remove(player);
        infectedPlayers.remove(player);
        deadPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = Infected.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
    }

    // kill a player
    public void killPlayer(Player player, Player killer) {
        if (hiddenInfected == player) {
            infectedFound = true;
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + "" + ChatColor.BOLD + player.getName() + " has been discovered!");
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + "Infected players will no longer respawn.");
            BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_BAT_TAKEOFF, 1.5f);
            // alert survivors
            player.getWorld().strikeLightning(player.getLocation());
            for(Player infectedPlayer : infectedPlayers) {
                infectedPlayer.getWorld().strikeLightning(infectedPlayer.getLocation());
            }

            hiddenInfected = null;
            if (killer != null && alivePlayers.contains(killer)) {
                infectedKills.compute(killer, (k, v) -> (v == null) ? 1 : v+1);
            }
        } else if (infectedPlayers.contains(player)) {
            if (infectedFound) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + "An infected has been killed!");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_ZOMBIE_DEATH, 1.65f);
                player.sendTitle(ChatColor.RED + "Eliminated!", null, 0, 20, 5);
                spectatePlayerPrep(player);
            } else {
                player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_DEATH, 1, 1.65f);
                respawnPlayerPrep(player);
            }
            player.sendMessage(ChatColor.DARK_GRAY + "You have killed " + ((survivorKills.get(player) == null)? 0:survivorKills.get(player))
                    + " survivors and killed " + ((infectedKills.get(player) == null)? 0:infectedKills.get(player)) + " infected.");
            if (killer != null && alivePlayers.contains(killer)) {
                infectedKills.compute(killer, (k, v) -> (v == null) ? 1 : v+1);
            }
            gameWinCheck();
            return;
        }
        if(alivePlayers.contains(player)) {
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + ", has been infected!");
            BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_PLAYER_DEATH, 1f);
            if (killer != null && (infectedPlayers.contains(killer) || hiddenInfected == killer)) {
                survivorKills.compute(killer, (k, v) -> (v == null) ? 1 : v+1);
            }
        }
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
        respawnPlayerPrep(player);
        gameWinCheck();
    }

    // check to see if this map has a player
    public boolean hasPlayer(Player player) {
        return waitingPlayers.contains(player) || alivePlayers.contains(player) || deadPlayers.contains(player) ||
                respawnPlayers.contains(player) || infectedPlayers.contains(player) || hiddenInfected == player;
    }

    public int getPlayerCount() {
        if (hiddenInfected != null) {
            return waitingPlayers.size() + deadPlayers.size() + alivePlayers.size() + respawnPlayers.size() +
                    infectedPlayers.size() + 1;
        } else {
            return waitingPlayers.size() + deadPlayers.size() + alivePlayers.size() + respawnPlayers.size() +
                    infectedPlayers.size();
        }
    }

    private boolean isGameReady() {
        return minPlayers <= waitingPlayers.size();
    }

    private void startCountdown() {
        // end if game has started or the game is not ready
        if (!isGameReady() || gameStarted) {
            return;
        }
        // do not start the counter if the counter is already running
        if (startCounter != null && !startCounter.isCancelled()) {
            return;
        }
        // we can finally start the counter
        startTimerCount = Infected.getSettings().getGameCountdown();
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "There are enough players to start the match.");
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
        startCounter = Bukkit.getScheduler().runTaskTimer(Infected.getPlugin(), () -> {
            if (startTimerCount == 0 && isGameReady()) {
                // timer has reached 0, it is show time
                startGraceCountdown();
                gameStarted = true;
                worldStatusRefresh();
                startCountdownBar.setVisible(false);
                stopStartCountdown();
                return;
            } else if (startTimerCount == 0) {
                // if the game was not ready and the count is at 0 we want to cancel
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                startCountdownBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
                startCountdownBar.setProgress(1.0);
                stopStartCountdown();
                return;
            }
            if (startTimerCount == 1) {
                startCountdownBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " second...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " second.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else if (startTimerCount <= 10) {
                startCountdownBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " seconds.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else {
                startCountdownBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
            }
            startCountdownBar.setProgress((double) startTimerCount / 30);
            if (waitingPlayers.size() >= maxPlayers && startTimerCount > 10) {
                startTimerCount = Infected.getSettings().getGameCountdownFast();
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game full! Starting match in " + startTimerCount + 's');
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            } else {
                startTimerCount--;
            }
        }, 0, 20);
    }

    // stop the starter countdown
    private void stopStartCountdown() {
        if (startCounter != null && !startCounter.isCancelled()) {
            startCounter.cancel();
            if (!isGameReady() && !gracePeriod) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            }
            startCountdownBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
            startCountdownBar.setProgress(1.0);
        }
    }

    // start the grace period
    public void startGraceCountdown() {
        gracePeriod = true;
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 2);

        // move players into the arena and select a random infected
        Set<Player> livingToMove = new HashSet<>(getWaitingPlayers());
        Iterator<Player> iterator = getWaitingPlayers().iterator();
        // set up random
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        // get to the random player
        for (int i = 0; i < (random.nextInt(livingToMove.size())); i++) {
            iterator.next();
        }
        Player infectedPlayerToMove = iterator.next();
        livingToMove.remove(infectedPlayerToMove);

        for (Player playerToMove : livingToMove) {
            survivorPlayerPrep(playerToMove);
        }

        hiddenInfectedPlayerPrep(infectedPlayerToMove);

        gracePeriodTime = Infected.getSettings().getGracePeriod();
        // delay 20 ticks before counting the grace period
        gracePeriodCounter = Bukkit.getScheduler().runTaskTimer(Infected.getPlugin(), () -> {
            if (gracePeriodTime == 0) {
                for (Player gamePlayer : alivePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "GO!", "", 0, 10, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 2);
                }
                for (Player gamePlayer : infectedPlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "GO!", "", 0, 10, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 2);
                }
                if (hiddenInfected != null) {
                    hiddenInfected.sendTitle(ChatColor.GOLD + "GO!", "", 0, 10, 5);
                    hiddenInfected.playSound(hiddenInfected.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 2);
                }
                startGame();
                winCounterClock();
                stopGraceCountdown();
                return;
            } else if (gracePeriodTime == 1) {
                for (Player gamePlayer : alivePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " second", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
                for (Player gamePlayer : infectedPlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " second", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
                if (hiddenInfected != null) {
                    hiddenInfected.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " second", 0, 20, 5);
                    hiddenInfected.playSound(hiddenInfected.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
            } else {
                for (Player gamePlayer : alivePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " seconds", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
                if (hiddenInfected != null) {
                    hiddenInfected.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " seconds", 0, 20, 5);
                    hiddenInfected.playSound(hiddenInfected.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
                for (Player gamePlayer : infectedPlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " seconds", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
            }
            gracePeriodTime--;
        }, 20, 20);
    }

    // Stop the grace period counter
    private void stopGraceCountdown() {
        gracePeriodCounter.cancel();
    }

    // Win counter clock
    private void winCounterClock() {
        survivorWinTime = initialSurvivorWinTime;
        survivorWinCountdownBar.setProgress((double)survivorWinTime/(double)initialSurvivorWinTime);
        for (Set<Player> players : new HashSet<>(Arrays.asList(infectedPlayers, alivePlayers, waitingPlayers))) {
            for (Player player : players) {
                survivorWinCountdownBar.addPlayer(player);
            }
        }
        if (hiddenInfected != null) {
            survivorWinCountdownBar.addPlayer(hiddenInfected);
        }
        winCounterClock = Bukkit.getScheduler().runTaskTimer(Infected.getPlugin(), () -> {
            survivorWinTime--;
            if (survivorWinTime <= 0) {
                gameWinCheck();
                stopWinCounterClock();
            }
            survivorWinCountdownBar.setProgress((double)survivorWinTime/(double)initialSurvivorWinTime);
            survivorWinCountdownBar.setTitle(ChatColor.RED + "Survivors win in " + survivorWinTime + "s");
        }, 20L, 20L);
    }

    // stop the survivor win countdown
    private void stopWinCounterClock() {
        winCounterClock.cancel();
        survivorWinCountdownBar.removeAll();
    }

    // the countdown at the end of the game until the game closes
    private void endGameCountdown() {
        winCounterClock.cancel();
        survivorWinCountdownBar.removeAll();
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.BLUE + "Match is closing in 10 seconds. Use /leave to go back to spawn.");
        if (!Infected.getPlugin().isEnabled()) {
            return;
        }
        Bukkit.getScheduler().runTaskLater(Infected.getPlugin(), () -> Infected.getInstanceManager().purgeInstance(this), 20 * 10L);
    }

    // start the game
    private void startGame() {
        pvpEnabled = true;
    }

    // save player score tallies
    private void saveScoreTallies(Player player) {
        ScoreManager sm = GameCore.getScoreManager();
        if (infectedKills.get(player) != null) {
            sm.modifyScore(player, Infected.getInfectedGame(), "Infected_Kills", infectedKills.remove(player), ScoreFunction.ADD);
        }
        if (survivorKills.get(player) != null) {
            sm.modifyScore(player, Infected.getInfectedGame(), "Survivor_Kills", survivorKills.remove(player), ScoreFunction.ADD);
        }
    }

    // check if the win condition has been met. If so end the game.
    private void gameWinCheck() {
        if ((alivePlayers.isEmpty() || survivorWinTime <= 0 || (infectedPlayers.isEmpty() && hiddenInfected == null)) && gameStarted && respawnPlayers.isEmpty()) {
            if (!gameEnded) {
                gameEnded = true;
                if (alivePlayers.isEmpty()) {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + "" + ChatColor.BOLD + "The Infected have won the game.");
                    for (Player player : alivePlayers) {
                        player.sendMessage(ChatColor.DARK_GRAY + "You have killed " + ((survivorKills.get(player) == null)? 0:survivorKills.get(player))
                                + " survivors and killed " + ((infectedKills.get(player) == null)? 0:infectedKills.get(player)) + " infected.");
                    }
                    for (Player player : infectedPlayers) {
                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, Infected.getInfectedGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, Infected.getInfectedGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You have killed " + ((survivorKills.get(player) == null)? 0:survivorKills.get(player))
                                + " survivors and killed " + ((infectedKills.get(player) == null)? 0:infectedKills.get(player)) + " infected.");
                    }
                    if (hiddenInfected != null) {
                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(hiddenInfected, Infected.getInfectedGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(hiddenInfected, Infected.getInfectedGame(), "Wins").get());
                            hiddenInfected.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            hiddenInfected.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        hiddenInfected.sendMessage(ChatColor.DARK_GRAY + "You have killed " + ((survivorKills.get(hiddenInfected) == null)? 0:survivorKills.get(hiddenInfected))
                                + " survivors and killed " + ((infectedKills.get(hiddenInfected) == null)? 0:infectedKills.get(hiddenInfected)) + " infected.");
                    }
                } else {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.AQUA + "" + ChatColor.BOLD + "The Survivors have won the game.");
                    for (Player player : alivePlayers) {
                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, Infected.getInfectedGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, Infected.getInfectedGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You have killed " + ((survivorKills.get(player) == null)? 0:survivorKills.get(player))
                                + " survivors and killed " + ((infectedKills.get(player) == null)? 0:infectedKills.get(player)) + " infected.");
                    }
                    for (Player player : infectedPlayers) {
                        player.sendMessage(ChatColor.DARK_GRAY + "You have killed " + ((survivorKills.get(player) == null)? 0:survivorKills.get(player))
                                + " survivors and killed " + ((infectedKills.get(player) == null)? 0:infectedKills.get(player)) + " infected.");
                    }
                    if (hiddenInfected != null) {
                        hiddenInfected.sendMessage(ChatColor.DARK_GRAY + "You have killed " + ((survivorKills.get(hiddenInfected) == null)? 0:survivorKills.get(hiddenInfected))
                                + " survivors and killed " + ((infectedKills.get(hiddenInfected) == null)? 0:infectedKills.get(hiddenInfected)) + " infected.");
                    }
                }
                for (Player worldPlayer : getGameWorld().getBukkitWorld().getPlayers()) {
                    if (hasPlayer(worldPlayer)) {
                        spectatePlayerPrep(worldPlayer);
                    }
                }
                endGameCountdown();
            }
        }
    }

    // refresh the gameWorld status
    private void worldStatusRefresh() {
        if (gameStarted) {
            getGameWorld().setStatus(GameStatus.RUNNING_CLOSED);
            return;
        }

        if (getPlayerCount() >= maxPlayers) {
            getGameWorld().setStatus(GameStatus.WAITING_CLOSED);
        } else {
            getGameWorld().setStatus(GameStatus.WAITING_OPEN);
        }
        getGameWorld().setPlayerCount(getPlayerCount());
        worldDespawnTimer();
    }

    // start the world despawn time if conditions are met
    private void worldDespawnTimer() {
        if (!Infected.getPlugin().isEnabled()) {
            return;
        }
        if (worldDespawnTimer == null || worldDespawnTimer.isCancelled()) {
            if (getPlayerCount() == 0) {
                worldDespawnTimer = Bukkit.getScheduler().runTaskLater(Infected.getPlugin(), () -> {
                    if (getPlayerCount() == 0) {
                        Infected.getInstanceManager().purgeInstance(this);
                    }
                }, 20 * Infected.getSettings().getWorldDespawnTime());
            }
        } else if (worldDespawnTimer != null && !worldDespawnTimer.isCancelled() && getPlayerCount() != 0) {
            worldDespawnTimer.cancel();
        }
    }

    // getters
    public Set<Player> getWaitingPlayers() {
        return waitingPlayers;
    }

    public Set<Player> getGamePlayers() {
        return alivePlayers;
    }

    public Set<Player> getInfectedPlayers() {
        return infectedPlayers;
    }

    public Player getHiddenInfectedPlayer() {
        return hiddenInfected;
    }

    public Set<Player> getDeadPlayers() {
        return deadPlayers;
    }

    public boolean pvpEnabled() {
        return pvpEnabled;
    }

    private Location getRandomSpawn() {
        for (int u = 0; u < 50; u++) {
            List<Region> spawnRegions = Infected.getSettings().getMapConfig(getGameWorld().getMap()).getSpawnRegions();
            Random rnd = GameCore.getGlobalRandom().getRandom();
            if (spawnRegions.isEmpty()) {
                return Infected.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
            }
            Region selectedReg = spawnRegions.get(rnd.nextInt(spawnRegions.size()));
            int selX = selectedReg.getPosition1().getBlockX() + rnd.nextInt((int) selectedReg.getWidth());
            int selZ = selectedReg.getPosition1().getBlockZ() + rnd.nextInt((int) selectedReg.getDepth());
            for (int i = 0; i < (selectedReg.getHeight()+1); i++) {
                Block block = getGameWorld().getBukkitWorld().getBlockAt(selX, selectedReg.getPosition1().getBlockY()+i, selZ);
                Block upBlock = block.getRelative(BlockFace.UP);
                Block downBlock = block.getRelative(BlockFace.DOWN);
                if (downBlock.getType().isSolid()) {
                    if (upBlock.isPassable() && block.isPassable() && !(block.getType().equals(Material.FIRE) || block.getType().equals(Material.LAVA))) {
                        return new Location(getGameWorld().getBukkitWorld(), selX + 0.5f, selectedReg.getPosition1().getY()+i, selZ + 0.5f);
                    }
                }
            }
        }
        Bukkit.getLogger().info("Infected could not find spawn location for player.");
        return Infected.getSettings().getMapConfig(getGameWorld().getMap()).getSpawnRegions().get(0).getPosition1().getLocation();
    }

    public boolean sameTeam(Player player1, Player player2) {
        if (alivePlayers.contains(player1) && alivePlayers.contains(player2)) {
            return true;
        }
        return (infectedPlayers.contains(player1) || hiddenInfected == player1) && (infectedPlayers.contains(player2) || hiddenInfected == player2);
    }
}

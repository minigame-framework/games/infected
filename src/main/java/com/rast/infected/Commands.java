package com.rast.infected;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Commands implements CommandExecutor, TabCompleter {

    private final List<String> subcommands = new ArrayList<>(Arrays.asList("setkit", "getkit", "kitlist"));

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        Game game = GameCore.getGameMaster().getPlayerGame((Player) sender);

        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please select a subcommand!");
            return true;
        }

        switch (args[0]) {
            case "setkit":
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.RED + "This subcommand requires an argument. The kit name.");
                    break;
                }
                if (!(args[1].equals("Survivor") || args[1].equals("Infected") || args[1].equals("Hidden-Infected"))) {
                    sender.sendMessage(ChatColor.RED + "You can only choose the pre-defined kit Survivor or Infected");
                    break;
                }
                try {
                    Infected.getKits().deleteKit(args[1]);
                    Infected.getKits().saveKit(args[1], ((Player) sender).getInventory(), Material.AIR);
                    sender.sendMessage(ChatColor.GOLD + "Kit " + args[1] + " has been saved.");
                } catch (IOException e) {
                    sender.sendMessage(ChatColor.GOLD + "File error occurred, kit could not be saved.");
                }
                break;
            case "getkit":
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.RED + "This subcommand requires a kit name.");
                    break;
                }
                if (!(args[1].equals("Survivor") || args[1].equals("Infected") || args[1].equals("Hidden-Infected"))) {
                    sender.sendMessage(ChatColor.RED + "You can only choose the pre-defined kit Survivor or Infected");
                    break;
                }
                sender.sendMessage(ChatColor.GOLD + "Kit " + args[1] + " has been equipped.");
                ((Player) sender).getInventory().setContents(Infected.getKits().loadKit(args[1]).getContents());
                break;
            case "kitlist":
                sender.sendMessage(ChatColor.GOLD + "List of kits:");
                StringBuilder builder = new StringBuilder(ChatColor.YELLOW.toString());
                for (String kitName : Infected.getKits().kitList()) {
                    builder.append(kitName);
                    builder.append(" | ");
                }
                sender.sendMessage(builder.toString());
                break;
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> tabList = new ArrayList<>();

        if (!(sender instanceof Player)) {
            return tabList;
        }

        if (args.length == 1) {
            return subcommands;
        }

        switch (args[0]) {
            case "setkit":
            case "getkit":
                tabList.add("Survivor");
                tabList.add("Infected");
                tabList.add("Hidden-Infected");
        }
        return tabList;
    }
}

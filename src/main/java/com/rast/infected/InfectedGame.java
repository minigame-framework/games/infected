package com.rast.infected;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameInstance;
import com.rast.gamecore.GameWorld;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class InfectedGame extends Game {

    public InfectedGame(String name, List<String> mapList, boolean canJoinWhenRunning, JavaPlugin plugin) {
        super(name, mapList, canJoinWhenRunning, plugin);
    }

    @Override
    public String getChatFormat(Player player) {
        if (player.getGameMode().equals(GameMode.SPECTATOR) || player.getGameMode().equals(GameMode.ADVENTURE)) {
            return Infected.getSettings().getLocalChatFormat().replace("%color%", ChatColor.GRAY.toString());
        }
        if (((InfectedInstance) Infected.getInstanceManager().getInstanceFromPlayer(player)).getInfectedPlayers().contains(player)) {
            return Infected.getSettings().getLocalChatFormat().replace("%color%", ChatColor.RED.toString());
        }
        return Infected.getSettings().getLocalChatFormat().replace("%color%", ChatColor.AQUA.toString());
    }

    @Override
    public GameInstance getNewGameInstance(GameWorld gameWorld) {
        return new InfectedInstance(gameWorld);
    }
}
